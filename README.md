# EXOphysics

A particle simulator & life simulator that runs in the browser!

There are many different variants of this, all in stand-alone HTML files.
Select another branch to view the variants.  There's a good overview in the
[exhibition](https://exophysics.codeberg.page/exhibition.html).

- License: Public Domain
- Website: https://exophysics.codeberg.page
- Author: Roman Zimbelmann
